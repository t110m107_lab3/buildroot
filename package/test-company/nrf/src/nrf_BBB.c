#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <linux/limits.h>
#include "MQTTAsync.h"

#include "support.h"
#include "nrf24.h"

#define TX 1
#define RX 0

/* MQTT */
#define ADDRESS     ""
#define QOS         0
#define TIMEOUT     10000L

#define MQTT_CLIENTID   ""
#define MQTT_USERNAME   ""
#define MQTT_PASSWORD   ""

#define MQTT_TEMPERATURE_TOPIC_LOCAL    ""
#define MQTT_TEMPERATURE_TOPIC_REMOTE   ""

/* W1 */
#define BASE_DIR "/sys/bus/w1/devices/"
#define TEMP_UNKNOWN 0xff
#define MAX_BUF 16

MQTTAsync client;
MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;

volatile MQTTAsync_token deliveredtoken;
int is_connected = 0;
int is_finished = 0;

void connection_lost(void *context, char *cause) {
    printf("\nConnection lost\n");
    printf("Cause: %s\n", cause);
    is_connected = 0;
    is_finished = 1;
}

void on_send(void* context, MQTTAsync_successData* response) {
    /*printf("Message with token value %d delivery confirmed\n", response->token);*/
    return;
}

void on_connect(void* context, MQTTAsync_successData* response) {
    printf("Successful connection\n");
    is_connected = 1;
}

void on_connect_failure(void* context, MQTTAsync_failureData* response) {
    printf("Connect failed, rc %d\n", response ? response->code : 0);
    is_finished = 1;
}

float read_temp() {
    DIR *dir;
    struct dirent *dirent;
    char buf[MAX_BUF];
    char path[PATH_MAX];
    FILE *fd;
    float value = TEMP_UNKNOWN;

    dir = opendir(BASE_DIR);
    if (!dir)
        return value;

    while ((dirent = readdir(dir)) != NULL) {
        if (dirent->d_type != DT_LNK)
            continue;

        snprintf(path, sizeof(path), "%s%s/temperature", BASE_DIR, dirent->d_name);

        fd = fopen(path, "r");
        if (!fd)
            continue;

        if (fgets(buf, MAX_BUF, fd) != NULL) {
            fclose(fd);
            value = atoi(buf) / 1000.0;
            break;
        }

        fclose(fd);
    }

    closedir(dir);

    return value;
}

void print_values(int pipe_nrf, char *payload) {
    printf("[REMOTE] pipe: %d\tpayload: %s\n", (int)pipe_nrf, payload);
    MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    pubmsg.payload = payload;
    pubmsg.payloadlen = strlen(payload);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    MQTTAsync_sendMessage(client, MQTT_TEMPERATURE_TOPIC_REMOTE, &pubmsg, &opts);

    /* XXX: read local temperature as well */
    char temp_str[20];
    float temp;

    temp = read_temp();
    if (temp != TEMP_UNKNOWN)
        snprintf(temp_str, 20, "%.2f", temp);
    else
        snprintf(temp_str, 20, "n/a");
    printf("[LOCAL] payload: %s\n", temp_str);

    MQTTAsync_message pubmsg2 = MQTTAsync_message_initializer;
    pubmsg2.payload = temp_str;
    pubmsg2.payloadlen = strlen(temp_str);
    pubmsg2.qos = QOS;
    pubmsg2.retained = 0;
    MQTTAsync_sendMessage(client, MQTT_TEMPERATURE_TOPIC_LOCAL, &pubmsg2, &opts);
}


int main() {
    MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
    int rc;

    printf("!!!Hello NRF!!!\n");

    MQTTAsync_create(&client, ADDRESS, MQTT_CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTAsync_setCallbacks(client, NULL, connection_lost, NULL, NULL);

    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    conn_opts.onSuccess = on_connect;
    conn_opts.onFailure = on_connect_failure;
    conn_opts.context = client;
    conn_opts.username = MQTT_USERNAME;
    conn_opts.password = MQTT_PASSWORD;

    opts.onSuccess = on_send;
    opts.context = client;

    if ((rc = MQTTAsync_connect(client, &conn_opts)) != MQTTASYNC_SUCCESS) {
        printf("Failed to start connect, return code %d\n", rc);
        return rc;
    }

    /* XXX: wait for connection to mqtt server */
    while (!is_connected && !is_finished)
        usleep(100000);

    runRadio(RX, print_values);

    MQTTAsync_destroy(&client);

    return 0;
}
