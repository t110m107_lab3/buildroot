NRF_VERSION = 1.0
NRF_SITE = $(TOPDIR)/package/test-company/nrf/src
NRF_SITE_METHOD = local
NRF_DEPENDENCIES += paho-mqtt-c

NRF_SRCS = nrf24.c nrf_BBB.c support.c

define NRF_EXTRACT_CMDS
	cp $(NRF_SITE)/*.c $(NRF_SITE)/*.h $(@D)/
endef

define NRF_BUILD_CMDS
	(cd $(@D); \
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -Wall -Os -c nrf24.c -o nrf24.o; \
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -Wall -Os -c support.c -o support.o; \
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -Wall -Os -c nrf_BBB.c -o nrf_BBB.o; \
	$(TARGET_CC) $(TARGET_CFLAGS) $(TARGET_LDFLAGS) -Wall -Os -s nrf24.o support.o nrf_BBB.o -o nrf -lpaho-mqtt3a)
endef

define NRF_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/nrf $(TARGET_DIR)/usr/bin/nrf
endef

$(eval $(generic-package))
